import os
import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog
from designer_qt5.Ui_AboutDialog import Ui_AboutDialog
from qt5_t5darkstyle import darkstyle_css

from helpers.constants import CURRENT_VERSION


class AboutDialog(QDialog, Ui_AboutDialog):
    def __init__(self):
        super(AboutDialog, self).__init__()
        self.setupUi(self)
        self.setStyleSheet(darkstyle_css())
        self.label.setText(self.label.text().replace("$version", CURRENT_VERSION))
        self.label_4.mousePressEvent = self.emailClicked
        self.label_5.mousePressEvent = self.licenceClicked
        self.setModal(True)


    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def emailClicked(self, ignore=None):
        url = QUrl('mailto:t-5@t-5.eu')
        QDesktopServices().openUrl(url)


    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def licenceClicked(self, ignore=None):
        url = QUrl(os.path.join(os.path.dirname(sys.argv[0]), "licence.txt"))
        QDesktopServices().openUrl(url)
