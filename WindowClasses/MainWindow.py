import os
import sys
from tempfile import mkstemp
from time import sleep, time

from PyQt5 import QtCore
from PyQt5.QtCore import QSettings, Qt, pyqtSignal, QTimer, QCoreApplication
from PyQt5.QtGui import QPixmap, QCursor
from PyQt5.QtWidgets import QMainWindow, QLabel, QMessageBox
from designer_qt5.Ui_MainWindow import Ui_MainWindow

from CustomWidgets.VolumeKnob import VolumeKnob
from WindowClasses.AboutDialog import AboutDialog
from helpers.GpioThread import GpioThread
from helpers.HttpServerThread import HttpServerThread
from helpers.styles import STYLESHEET_MUTEBUTTONS


class MainWindow(QMainWindow, Ui_MainWindow):


    KNOB_HEIGHT_PERCENT = 85
    INDICATOR_WIDTH = 10
    TABWIDGET_TABBAR_HEIGHT = 24

    HIDE_THINGS_AFTER = 60
    HIDE_THINGS_FADEOUT = 5

    signal_mute = pyqtSignal()
    signal_powerGoodStateChanged = pyqtSignal(int)
    signal_rampupVolume = pyqtSignal(int)
    signal_readIniSettings = pyqtSignal()
    signal_setVolume = pyqtSignal(int)
    signal_stepVolume = pyqtSignal(int, int)
    signal_volumeDown = pyqtSignal()
    signal_volumeUp = pyqtSignal()


    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.setWindowFlag(Qt.FramelessWindowHint)
        ## module import checks
        for modulename in ("PyQt5",):
            self._importCheck(modulename)
        # condition to wait on volume change signals
        self._condition_setVolume = QtCore.QWaitCondition()
        # state variables:
        self._dirty = False
        self._lastInputTimestamp = 0
        self._rampupInterrupt = False
        self._volume = 0                                  # 0: -∞ dB, 64: 0 dB
        self._swVolumes = [0, 0, 0, 0, 0]                 # 0 to -63 dB
        self._hwVolumes = [0, 0, 0, 0, 0]                 # bcd encoded mute relay states
        self._muteStates = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] # 0: muted, 1: unmuted
        self._powerState = 0                              # 0: bad, 1: good
        self._minVolume = 0
        # settings and initialization
        self._settings = QSettings("VolCtrlDR", "VolCtrlDR")
        self._setupTabWidget()
        # setup and start webserver
        self._mutex = QtCore.QMutex()
        self._httpd = HttpServerThread(parent=self,
                                       mtx=self._mutex,
                                       cond_volume=self._condition_setVolume)
        self._httpd.start()
        # setup and start gpio thread
        self._gpioThread = GpioThread(parent=self)
        self._gpioThread.start()
        # hide things labels
        #self._hideTopBarLabel = QLabel(self)
        #self._hideTopBarLabel.mousePressEvent = self.tabWidget.mousePressEvent
        #self._hideTopBarLabel.setFixedSize(796, 38)
        #self._hideTopBarLabel.setStyleSheet("background-color: rgba(0, 0, 0, 0);")
        #self._hideTopBarLabel.move(2, 0)
        self._hideLeftBottomLabel = QLabel(self)
        self._hideLeftBottomLabel.setFixedSize(200, 100)
        self._hideLeftBottomLabel.move(4, 376)
        self._hideLeftBottomLabel.setStyleSheet("background-color: rgba(0, 0, 0, 0);")
        self._hideRightBottomLabel = QLabel(self)
        self._hideRightBottomLabel.setFixedSize(200, 100)
        self._hideRightBottomLabel.move(596, 376)
        self._hideRightBottomLabel.setStyleSheet("background-color: rgba(0, 0, 0, 0);")
        # setup hide things timer
        self._hideThingsTimer = QTimer()
        self._hideThingsTimer.setInterval(100)
        self._hideThingsTimer.start()
        # setup write ini settings timer
        self._writeInitSettingsTimer = QTimer()
        self._writeInitSettingsTimer.setInterval(10000)
        self._writeInitSettingsTimer.start()
        # finally connect all signals
        self._connectSignals()


    def closeEvent(self, evt):
        """ do stuff necessary before closing the main window """
        self._rampupInterrupt = True
        self._writeIniSettings()


    def getMinVolume(self):
        return self._minVolume


    def getMuteStates(self):
        return self._muteStates


    def getPowerState(self):
        return self._powerState


    def getVolume(self):
        return self._volume


    def mouseMoveEvent(self, _):
        self._inputHappened()


    def mousePressEvent(self, _):
        self._inputHappened()


    def onExit(self):
        self.close()


    def onIndividualGainChanged(self):
        self._dirty = True
        self._inputHappened()
        miv = self.spinBoxGain1.value()
        miv = min(miv, self.spinBoxGain3.value())
        miv = min(miv, self.spinBoxGain5.value())
        miv = min(miv, self.spinBoxGain7.value())
        miv = min(miv, self.spinBoxGain9.value())
        self._minVolume = -miv + 1
        self.spinBoxOverallGain.blockSignals(True)
        self.spinBoxOverallGain.setMinimum(-64 + self._minVolume)
        self.spinBoxOverallGain.blockSignals(False)
        self._volumeKnob.setMinValue(self._minVolume)
        if self._volume == 0:
            return
        if self._volume < self._minVolume:
            self._setVolume(self._minVolume)
        else:
            self._setVolume(self._volume)


    def onMute(self):
        self._inputHappened()
        self._setVolume(0)


    def onMuteButtonChecked(self):
        newStates = []
        for i in range(1, 11):
            checked = getattr(self, "buttonMuteCh%d" % i).isChecked()
            if checked:
                newStates.append(0)
            else:
                newStates.append(1)
        self._setMuteStates(newStates)


    def onOverallGainChanged(self):
        self._inputHappened()
        self._setVolume(64 + self.spinBoxOverallGain.value())


    def onPowerGoodStateChanged(self, state):
        self._powerState = state
        if state:
            self.labelPowerLed.setPixmap(QPixmap(":/VolumeKnob/led_green.png"))
            self.labelPowerStatus.setText("Good")
            self.labelPowerStatus.setStyleSheet("color: #00ff00;")
        else:
            self.labelPowerLed.setPixmap(QPixmap(":/VolumeKnob/led_red.png"))
            self.labelPowerStatus.setText("Bad")
            self.labelPowerStatus.setStyleSheet("color: #ff0000;")


    def onRampupVolume(self, volume):
        for v in range(self._minVolume, volume + 1):
            if self._rampupInterrupt:
                break
            self._setVolume(v)
            for i in range(0, 30):  # sleep 300ms and process GUI events inbetween
                QCoreApplication.processEvents()
                sleep(0.01)


    def onReadIniSettings(self):
        self._readIniSettings()


    def onSetVolume(self, volume):
        self._inputHappened()
        if volume == 0:
            self._setVolume(0)
            return
        if volume < self._minVolume:
            volume = self._minVolume
        self._setVolume(volume)


    def onStepVolume(self, direction, steps):
        if direction:
            self.onVolumeUp(steps)
        else:
            self.onVolumeDown(steps)


    @staticmethod
    def onShowAboutDialog(_):
        """ show about dialog """
        dlg = AboutDialog()
        dlg.exec_()


    def onVolumeDown(self, steps=1):
        self._inputHappened()
        if self._volume == 0:
            return
        if self._volume <= self._minVolume:
            self._setVolume(0)
        else:
            self._setVolume(max(self._minVolume, self._volume - steps))


    def onVolumeKnobChanged(self):
        self._inputHappened()
        self._setVolume(self._volumeKnob.value())


    def onVolumeKnobClicked(self):
        self._inputHappened()


    def onVolumeUp(self, steps=1):
        self._inputHappened()
        if self._volume >= 64:
            return
        if self._volume == 0 and self._minVolume > 0:
            self._setVolume(self._minVolume)
        else:
            self._setVolume(min(64, self._volume + steps))


    def resizeEvent(self, event):
        if self.isHidden() or self.tabWidget.currentIndex() != 0:
            return
        h = int(round(self.volumeTab.height() * self.KNOB_HEIGHT_PERCENT / 100))
        x = int(round((self.width() - h) / 2))
        y = int(round((self.volumeTab.height() - h) / 2)) - self.TABWIDGET_TABBAR_HEIGHT
        self._volumeKnob.move(x, y)
        self._volumeKnob.setFixedSize(h, h)



    def showEvent(self, event):
        super().showEvent(event)
        QTimer.singleShot(100, self._readIniSettings)


    def _calculateAndUpdateSwVolumes(self):
        for i in range(0, 5):
            self._swVolumes[i] = max(0, self._volume + getattr(self, "spinBoxGain%d" % (i * 2 + 1)).value())
            getattr(self, "labelGain%d" % (i * 2 + 1)).setText(f'{self._swVolumes[i] - 64} dB')


    # noinspection PyUnresolvedReferences
    def _connectSignals(self):
        self.spinBoxGain1.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain3.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain5.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain7.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxGain9.valueChanged.connect(self.onIndividualGainChanged)
        self.spinBoxOverallGain.valueChanged.connect(self.onOverallGainChanged)
        self.signal_mute.connect(self.onMute)
        self.signal_powerGoodStateChanged.connect(self.onPowerGoodStateChanged)
        self.signal_rampupVolume.connect(self.onRampupVolume)
        self.signal_readIniSettings.connect(self.onReadIniSettings)
        self.signal_setVolume.connect(self.onSetVolume)
        self.signal_stepVolume.connect(self.onStepVolume)
        self.signal_volumeDown.connect(self.onVolumeDown)
        self.signal_volumeUp.connect(self.onVolumeUp)
        self.tabWidget.currentChanged.connect(self._inputHappened)
        self._hideThingsTimer.timeout.connect(self._hideThingsTimerHandler)
        self._writeInitSettingsTimer.timeout.connect(self._writeInitSettingsTimerHandler)
        self._logoLabel.mousePressEvent = self.onShowAboutDialog
        self._volumeKnob.valueChanged.connect(self.onVolumeKnobChanged)
        self._volumeKnob.clicked.connect(self.onVolumeKnobClicked)
        self._volumeKnob.doubleClicked.connect(self.onMute)
        for i in range(1, 11):
            getattr(self, "buttonMuteCh%d" % i).toggled.connect(self.onMuteButtonChecked)


    def _hideThingsLabelsSetOpacity(self, opacity):
        if opacity == 0.0:
            self._hideLeftBottomLabel.hide()
            self._hideRightBottomLabel.hide()
        else:
            ss = f"background-color: rgba(17, 17, 17, {opacity:.2f});"
            self._hideLeftBottomLabel.show()
            self._hideRightBottomLabel.show()
            self._hideLeftBottomLabel.setStyleSheet(ss)
            self._hideRightBottomLabel.setStyleSheet(ss)


    def _hideThingsTimerHandler(self):
        if self.tabWidget.currentIndex() != 0:
            return
        delta = time() - self._lastInputTimestamp
        if self.HIDE_THINGS_AFTER < delta < (self.HIDE_THINGS_AFTER + self.HIDE_THINGS_FADEOUT + 1):
            self._hideThingsLabelsSetOpacity(min((delta - self.HIDE_THINGS_AFTER) / 2, 1))


    def _importCheck(self, modulename):
        try:
            __import__(modulename)
        except ImportError:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            msg = "WARNING:\n"
            msg += "  Could not find python module '%s'.\n" % modulename
            msg += "  Should we try to install it for you?\n"
            msg += "  (This may take a few seconds or minutes...)"
            result = dlg.warning(self, "Dependencies warning", msg, QMessageBox.Yes | QMessageBox.No)
            if result == QMessageBox.Yes:
                break_system_packages = sys.version_info[1] >= 12 and "--break-system-packages" or ""
                ret = os.system("pip3 install %s %s" % (break_system_packages, modulename))
                if ret != 0:
                    msg = "ERROR: module '%s' not found.\n" % modulename
                    msg += "  Manual installation: 'pip3 install %s'" % modulename
                    dlg.critical(self, "ERROR loading dependencies!", msg)
                    print(msg, file=sys.stderr)
                    sys.exit(1)
                else:
                    msg = "SUCCESS:\n"
                    msg += "  The module '%s' was successfully installed.\n" % modulename
                    msg += "  The program will be restarted now..."
                    dlg.information(self, "Need to restart...", msg, QMessageBox.Ok)
                    fd, fn = mkstemp()
                    os.close(fd)
                    os.chmod(fn, 0o700)
                    f = open(fn, "w")
                    f.write("#!/bin/sh\nsleep 2\n/usr/bin/volctrldr\nrm -f %s\n" % fn)
                    f.close()
                    os.system("%s &" % fn)
                    sys.exit(0)
            else:
                sys.exit(1)


    def _inputHappened(self):
        self._hideThingsLabelsSetOpacity(0)
        self._lastInputTimestamp = time()
        self._rampupInterrupt = True


    def _readIniSettings(self):
        """ read ini settings """
        s = self._settings
        # preset volume to 0
        self._setVolume(0)
        # mute states
        try:
            v = s.value("muteStates", [0, 0, 0, 0, 0])
            if not isinstance(v, list) or len(v) != 10:
                v = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            v = list(map(lambda x: int(x), v))
        except ValueError:
            v = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for i, val in enumerate(v):
            if val < 0 or val > 1:
                val = 0
            getattr(self, "buttonMuteCh%d" % (i + 1)).setChecked(not bool(val))
        # individual attenuation offsets
        try:
            v = s.value("attenuationOffsets", [0, 0, 0, 0, 0])
            if not isinstance(v, list) or len(v) != 5:
                v = [0, 0, 0, 0, 0]
            v = list(map(lambda x: int(x), v))
        except ValueError:
            v = [0, 0, 0, 0, 0]
        for i, val in enumerate(v):
            if val < -63 or val > 0:
                val = 0
            getattr(self, "spinBoxGain%d" % (i * 2 + 1)).setValue(val)
        # volume
        try:
            v = int(s.value("volume", 0))
            if v != 0 and (v < self._minVolume or v > 64):
                v = 0
        except ValueError:
            v = 0
        self._rampupInterrupt = False
        if v == 0:
            self._setVolume(0)
        else:
            # 2do: wait for power good signal to become high
            # possibly via gpio thread...
            self.signal_rampupVolume.emit(v)


    def _setMuteStates(self, muteStates):
        self._dirty = True
        self._muteStates = muteStates
        for idx, state in enumerate(self._muteStates):
            res = state and ":/VolumeKnob/led_green.png" or ":/VolumeKnob/led_red.png"
            getattr(self, "labelLedCh%d" % (idx + 1)).setPixmap(QPixmap(res))
        # update hardware mute states:
        pass #2do (!!! volume == 0 -> mute all)


    def _setupTabWidget(self):
        self.tabWidget.setCurrentIndex(0)
        self.tabWidget.setTabText(0, "Volume Control")
        self.tabWidget.setTabText(1, "Mixer Controls")
        self._volumeKnob = VolumeKnob(self.tabWidget.currentWidget(), width=self.INDICATOR_WIDTH, steps=64, value=0)
        # led label
        self._logoLabel = QLabel(self)
        self._logoLabel.setScaledContents(True)
        self._logoLabel.setPixmap(QPixmap(":/MainWindow/t5_white_on_transparent.png"))
        self._logoLabel.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
        self._logoLabel.setFixedSize(38, 26)
        self._logoLabel.move(755, 7)
        self._logoLabel.setCursor(QCursor(Qt.PointingHandCursor))
        # set stylesheets for mute buttons
        for i in range(1, 11):
            getattr(self, "buttonMuteCh%d" % i).setStyleSheet(STYLESHEET_MUTEBUTTONS)


    def _setVolume(self, volume):
        if volume < 0 or volume > 64:
            raise ValueError("volume out of bounds: %r" % volume)
        print(f"new volume: {volume}")
        self._dirty = True
        self._volume = volume
        # update volume knob
        self._volumeKnob.blockSignals(True)
        self._volumeKnob.setValue(volume)
        self._volumeKnob.blockSignals(False)
        # update overall gain spinbox
        self.spinBoxOverallGain.blockSignals(True)
        self.spinBoxOverallGain.setValue(volume - 64)
        self.spinBoxOverallGain.blockSignals(False)
        # calculate individual sw volumes and update labels accordingly
        self._calculateAndUpdateSwVolumes()
        # calculate and set individual sw volumes
        pass #2do (!!! also call gpio handler)
        # finally wake the set volume condition
        self._condition_setVolume.wakeAll()


    def _writeIniSettings(self):
        """ write ini settings """
        s = self._settings
        s.setValue("muteStates", self._muteStates)
        s.setValue("minVolume", self._minVolume)
        s.setValue("volume", self._volume)
        s.setValue("attenuationOffsets", [
            self.spinBoxGain1.value(),
            self.spinBoxGain3.value(),
            self.spinBoxGain5.value(),
            self.spinBoxGain7.value(),
            self.spinBoxGain9.value()
        ])
        self._dirty = False


    def _writeInitSettingsTimerHandler(self):
        if self._dirty:
            self._writeIniSettings()
