#!/usr/bin/python3

"""
VolCtrlDR
===========
Software for the VolCtrlDR hardware controller

Main python (startup) file

GIT: https://gitlab.com/t-5/volctrldr_sw.git
"""

import sys

from PyQt5 import QtWidgets
from qt5_t5darkstyle import darkstyle_css

from WindowClasses.MainWindow import MainWindow
from helpers.styles import DARKSTYLE_CSS_COLORS, STYLESHEET_SPINBOX


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = MainWindow()
    form.setStyleSheet(darkstyle_css(DARKSTYLE_CSS_COLORS) + STYLESHEET_SPINBOX)
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
