CURRENT_VERSION = "0.23"

WEBSERVER_501_TEXT = "ERROR 501: Insufficient or wrong arguments."
WEBSERVER_HELPTEXT = """\
VolCtrlDR RemoteControl Interface
=================================

The following REQUESTs are valid:

No Parameters, Return is "OK":
------------------------------
/alive        Check if server is alive

No Parameters, Return is diverse:
---------------------------------
/get_status                  Get volume, powerState and muteStates as json dict
/get_volume                  Get volume as int (0-64, 0: -inf dB, 64: -0dB)
/volume_down                 Decrease Volume by 1dB, return new volume int
/volume_up                   Increase Volume by 1dB, return new volume int

One Parameter, Return is "OK":
------------------------------
/set_volume?volume=<int>     Set volume, must be between 0 and 64 incl.                           
"""
WEBSERVER_HOST = "0.0.0.0"
WEBSERVER_PORT = 8237 # T9: VCDR
