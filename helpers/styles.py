DARKSTYLE_CSS_COLORS = {
    "foreground_color":             "#ffffff",
    "background_color":             "#151515",
    "tooltip_background_color":     "#ffff77",
    "tooltip_border_color":         "#cccc55",
    "tooltip_color":                "#000000",
    "selection_background_color":   "#002200",
    "selection_highlight_color":    "#00aa00",
    "border_color":                 "#333333",
    "disabled_color":               "#bbbbbb",
    "edit_background_color":        "#000000",
    "scroll_background_color":      "#000000",
    "scroll_foreground_color":      "#003300",
    "button_background_color":      "#333333",
}

STYLESHEET_MUTEBUTTONS = """
QPushButton {
    background-color: #006600;
}
QPushButton:checked {
    background-color: #aa0000;
}
"""

STYLESHEET_SPINBOX = """
QAbstractSpinBox:up-button {
    width: 40px;
    background-color: transparent;
    subcontrol-origin: border;
    subcontrol-position: center right;
}

QAbstractSpinBox:down-button {
    width: 40px;
    background-color: transparent;
    subcontrol-origin: border;
    subcontrol-position: center left;
}

QAbstractSpinBox::up-arrow,
QAbstractSpinBox::up-arrow:disabled,
QAbstractSpinBox::up-arrow:off {
    image: url(:/MainWindow/spinbox_up_arrow.png);
    width: 30px;
    height: 30px;
}
QAbstractSpinBox::up-arrow:hover {
    image: url(:/MainWindow/spinbox_up_arrow.png);
}

QAbstractSpinBox::down-arrow,
QAbstractSpinBox::down-arrow:disabled,
QAbstractSpinBox::down-arrow:off {
    image: url(:/MainWindow/spinbox_down_arrow.png);
    width: 30px;
    height: 30px;
}
QAbstractSpinBox::down-arrow:hover {
    image: url(:/MainWindow/spinbox_down_arrow.png);
}
"""
