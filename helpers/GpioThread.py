from sys import stderr
from time import sleep, time

from PyQt5 import QtCore

try:
    import ASUS.GPIO as GPIO
except ImportError:
    GPIO = None


class GpioThread(QtCore.QThread):

    CLOCKPIN = 33
    DATAPIN = 35
    SWITCHPIN = 37
    POWERGOODPIN = 38
    PLUSPIN = 40

    CLOCKWISE = 0
    ANTICLOCKWISE = 1
    DEBOUNCE = 20

    SPEEDUPTIME1 = 0.15
    SPEEDUPTIME2 = 0.3
    SPEEDUPCOUNT1 = 5
    SPEEDUPSTEPS1 = 3
    SPEEDUPCOUNT2 = 10
    SPEEDUPSTEPS2 = 5

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self._running = False
        self._rotaryEventTimes = [0.0] * 20
        if GPIO is not None:
            print("GPIO setup... ", end="", file=stderr)
            GPIO.setmode(GPIO.BOARD)
            GPIO.setwarnings(False)
            GPIO.setup(self.CLOCKPIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.DATAPIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.SWITCHPIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.POWERGOODPIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.setup(self.PLUSPIN, GPIO.OUT)
            GPIO.output(self.PLUSPIN, 1)
            GPIO.add_event_detect(self.CLOCKPIN, GPIO.FALLING, callback=self._clockCallback, bouncetime=self.DEBOUNCE)
            GPIO.add_event_detect(self.SWITCHPIN, GPIO.FALLING, callback=self._switchCallback, bouncetime=self.DEBOUNCE)
            GPIO.add_event_detect(self.POWERGOODPIN, GPIO.BOTH, callback=self._powerGoodCallback)
            print("done.", file=stderr)
        else:
            print("WARNING: ASUS.GPIO module not found.", file=stderr)


    def run(self):
        self._running = True
        while self._running:
            sleep(0.1)


    def stop(self):
        self._running = False
        if GPIO is not None:
            GPIO.cleanup()
            GPIO.remove_event_detect(self.CLOCKPIN)
            GPIO.remove_event_detect(self.SWITCHPIN)
        self.wait()


    def _clockCallback(self, _):
        self._rotaryEventTimes.pop(0)
        self._rotaryEventTimes.append(time())
        steps = 1
        for idx in range(-1, -self.SPEEDUPCOUNT1, -1):
            delta = time() - self._rotaryEventTimes[idx]
            if delta > self.SPEEDUPTIME1:
                break
            if idx == -self.SPEEDUPCOUNT1 + 1:
                steps = self.SPEEDUPSTEPS1
        for idx in range(-1, -self.SPEEDUPCOUNT2, -1):
            delta = time() - self._rotaryEventTimes[idx]
            if delta > self.SPEEDUPTIME2:
                break
            if idx == -self.SPEEDUPCOUNT2 + 1:
                steps = self.SPEEDUPSTEPS2
        if GPIO.input(self.CLOCKPIN) == 0:
            self._emitToMainAppTwoArgs("signal_stepVolume", GPIO.input(self.DATAPIN), steps)


    def _emitToMainAppNoArgs(self, signal):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit()


    def _emitToMainAppOneArg(self, signal, arg):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit(arg)


    def _emitToMainAppTwoArgs(self, signal, arg1, arg2):
        qsignal = getattr(self.parent(), signal)
        qsignal.emit(arg1, arg2)


    def _powerGoodCallback(self):
        self._emitToMainAppOneArg("signal_powerGoodStateChanged", GPIO.input(self.POWERGOODPIN))


    def _switchCallback(self, _):
        self._emitToMainAppNoArgs("signal_mute")
