import math


def dbToGainFactor(db):
    return pow(10, db / 20.0)


def gainFactorToDb(gain):
    if gain == 0.0:
        return -1000000
    return 20 * math.log10(gain)


def prettyDbFromFactor(factor):
    if factor == 0.0:
        return "-∞ dB"
    return "%0.1f dB" % gainFactorToDb(factor)
